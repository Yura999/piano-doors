package ru.bardashev.doors.access.service;

import ru.bardashev.doors.access.web.model.dto.DoorAccessAttemptDto;

public interface DoorsService {

    void checkAndGo(DoorAccessAttemptDto attemptDto);

}
