package ru.bardashev.doors.access.service.impl;

import org.springframework.stereotype.Service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import ru.bardashev.doors.access.component.mapper.DoorAccessAttemptDtoMapper;
import ru.bardashev.doors.access.domain.repository.DoorAccessAttemptRepository;
import ru.bardashev.doors.access.enumeration.CheckResultCode;
import ru.bardashev.doors.access.exception.CheckResultException;
import ru.bardashev.doors.access.exception.CheckResultForbiddenException;
import ru.bardashev.doors.access.exception.CheckResultWrongDirectionException;
import ru.bardashev.doors.access.exception.CheckResultWrongRoomException;
import ru.bardashev.doors.access.service.DoorsService;
import ru.bardashev.doors.access.web.model.dto.DoorAccessAttemptDto;

@Slf4j
@Service
@RequiredArgsConstructor
public class DoorsServiceImpl implements DoorsService {

    private final DoorAccessAttemptRepository repository;
    private final DoorAccessAttemptDtoMapper mapper;

    @Override
    public void checkAndGo(DoorAccessAttemptDto attemptDto) {

        try {
            check(attemptDto);
            saveToDb(attemptDto, CheckResultCode.SUCCESS);
        } catch (Exception e) {
            var result = e instanceof CheckResultException ? ((CheckResultException) e).getResult() : CheckResultCode.ERROR;
            saveToDb(attemptDto, result);
            throw e;
        }
    }

    private void check(DoorAccessAttemptDto attemptDto) {
        if (attemptDto.getKeyId() % attemptDto.getRoomId() > 0) {
            throw new CheckResultForbiddenException();
        }
        var lastAttempt = repository.findLastSuccessfulAttempt(attemptDto.getKeyId());
        var currentRoomId = lastAttempt.isPresent() && Boolean.TRUE.equals(lastAttempt.get().getEntrance())
                ? lastAttempt.get().getRoomId()
                        : null;

        if (currentRoomId == null && Boolean.FALSE.equals(attemptDto.getEntrance())) {
            throw new CheckResultWrongDirectionException("Not in a room");
        }
        if (currentRoomId != null && Boolean.TRUE.equals(attemptDto.getEntrance())) {
            throw new CheckResultWrongDirectionException("Already in a room");
        }
        if (currentRoomId != null && Boolean.FALSE.equals(attemptDto.getEntrance()) && currentRoomId != attemptDto.getRoomId()) {
            throw new CheckResultWrongRoomException("Wrong room");
        }
    }

    private void saveToDb(DoorAccessAttemptDto attemptDto, CheckResultCode checkResult) {
        var attempt = mapper.map(attemptDto, checkResult);
        try {
            var result = repository.save(attempt);
            log.debug("Attempt saved: {}", result);
        } catch (Exception e) {
            log.error(String.format("Error writing to access journal: %s", attempt), e);
        }
    }

}
