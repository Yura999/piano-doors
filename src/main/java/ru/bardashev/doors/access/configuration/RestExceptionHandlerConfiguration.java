package ru.bardashev.doors.access.configuration;

import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.web.bind.annotation.ControllerAdvice;

import ru.bardashev.doors.access.configuration.handler.DefaultExceptionHandler;

@Order(Ordered.HIGHEST_PRECEDENCE)
@ControllerAdvice
public class RestExceptionHandlerConfiguration extends DefaultExceptionHandler {
}

