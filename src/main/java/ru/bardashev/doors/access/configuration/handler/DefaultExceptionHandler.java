package ru.bardashev.doors.access.configuration.handler;

import java.util.Optional;

import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR;


public class DefaultExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(Exception.class)
    protected ResponseEntity<Object> handleException(Exception ex) {
        HttpStatus code = null;
        String reason = null;
        Optional<ResponseStatus> status = Optional.ofNullable(AnnotationUtils.findAnnotation(ex.getClass(), ResponseStatus.class));

        if (status.isPresent()) {
            code = status.get().code();
            reason = status.get().reason();
        } else {
            code = INTERNAL_SERVER_ERROR;
            reason = "Internal error";
        }

        return buildResponseEntity(code, reason, ex);
    }

    @Override
    protected ResponseEntity<Object> handleExceptionInternal(Exception ex, Object body, HttpHeaders headers, HttpStatus status,
            WebRequest request) {

        if (ex instanceof MissingServletRequestParameterException) {
            return buildResponseEntity(BAD_REQUEST, "Missing parameter", ex);
        }
        return buildResponseEntity(status, "Unhandled exception", ex);
    }

    @Override
    protected ResponseEntity<Object> handleBindException(BindException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        return buildResponseEntity(BAD_REQUEST, "Validation error", ex);
    }

    protected ResponseEntity<Object> buildResponseEntity(HttpStatus httpStatus, String reason, Throwable ex) {
        var apiError = new ApiError(httpStatus, reason, ex);
        return new ResponseEntity<>(apiError, httpStatus);
    }
}
