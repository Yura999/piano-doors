package ru.bardashev.doors.access.enumeration;

public enum CheckResultCode {

    FORBIDDEN,
    WRONG_DIRECTION,
    WRONG_ROOM,
    ERROR,
    SUCCESS
}
