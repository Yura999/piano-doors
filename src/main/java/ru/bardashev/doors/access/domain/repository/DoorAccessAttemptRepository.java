package ru.bardashev.doors.access.domain.repository;

import java.util.Optional;
import java.util.UUID;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import ru.bardashev.doors.access.domain.entity.DoorAccessAttempt;
import ru.bardashev.doors.access.enumeration.CheckResultCode;

@Repository
public interface DoorAccessAttemptRepository extends CrudRepository<DoorAccessAttempt, UUID> {

    Optional<DoorAccessAttempt> findFirstByKeyIdAndResultOrderByAccessDateDesc(Integer keyId, CheckResultCode result);

    default Optional<DoorAccessAttempt> findLastSuccessfulAttempt(Integer keyId) {
        return findFirstByKeyIdAndResultOrderByAccessDateDesc(keyId, CheckResultCode.SUCCESS);
    }
}
