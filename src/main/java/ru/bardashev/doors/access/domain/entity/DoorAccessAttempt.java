package ru.bardashev.doors.access.domain.entity;

import java.time.LocalDateTime;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Type;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.EqualsAndHashCode.Include;
import ru.bardashev.doors.access.enumeration.CheckResultCode;


@Entity
@Data
@Table(name = "door_access_attempt")
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class DoorAccessAttempt {

    public DoorAccessAttempt() {
    }

    public DoorAccessAttempt(Integer roomId, Integer keyId, Boolean entrance, CheckResultCode result) {
        this.roomId = roomId;
        this.keyId = keyId;
        this.entrance = entrance;
        this.result = result;
    }

    @Id
    @Include
    @Type(type = "uuid-char")
    private UUID id;

    @NotNull
    @Column(name = "access_date")
    private LocalDateTime accessDate = LocalDateTime.now();

    @NotNull
    @Column(name = "room_id")
    private Integer roomId;

    @NotNull
    @Column(name = "key_id")
    private Integer keyId;

    @NotNull
    private Boolean entrance;

    @NotNull
    @Enumerated(value = EnumType.STRING)
    private CheckResultCode result;

    @PrePersist
    private void doCreate() {
        setId(UUID.randomUUID());
    }
}
