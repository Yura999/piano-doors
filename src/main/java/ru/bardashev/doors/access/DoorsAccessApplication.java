package ru.bardashev.doors.access;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DoorsAccessApplication {

	public static void main(String[] args) {
		SpringApplication.run(DoorsAccessApplication.class, args);
	}

}
