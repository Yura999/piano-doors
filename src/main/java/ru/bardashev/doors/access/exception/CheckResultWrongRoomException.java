package ru.bardashev.doors.access.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import ru.bardashev.doors.access.enumeration.CheckResultCode;

@ResponseStatus(code = HttpStatus.BAD_REQUEST, reason = "Wrong room")
public class CheckResultWrongRoomException extends CheckResultException {

    private static final long serialVersionUID = 1L;

    public CheckResultWrongRoomException() {
        super(CheckResultCode.WRONG_ROOM);
    }

    public CheckResultWrongRoomException(String message) {
        super(CheckResultCode.WRONG_ROOM, message);
    }

}
