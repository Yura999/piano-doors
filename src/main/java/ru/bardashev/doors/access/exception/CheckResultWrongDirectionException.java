package ru.bardashev.doors.access.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import ru.bardashev.doors.access.enumeration.CheckResultCode;

@ResponseStatus(code = HttpStatus.BAD_REQUEST, reason = "Wrong direction")
public class CheckResultWrongDirectionException extends CheckResultException {

    private static final long serialVersionUID = 1L;

    public CheckResultWrongDirectionException() {
        super(CheckResultCode.WRONG_DIRECTION);
    }

    public CheckResultWrongDirectionException(String message) {
        super(CheckResultCode.WRONG_DIRECTION, message);
    }

}
