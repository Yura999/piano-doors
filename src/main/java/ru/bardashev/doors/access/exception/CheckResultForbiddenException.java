package ru.bardashev.doors.access.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import ru.bardashev.doors.access.enumeration.CheckResultCode;

@ResponseStatus(code = HttpStatus.FORBIDDEN, reason = "Forbidden")
public class CheckResultForbiddenException extends CheckResultException {

    private static final long serialVersionUID = 1L;

    public CheckResultForbiddenException() {
        super(CheckResultCode.FORBIDDEN);
    }
}
