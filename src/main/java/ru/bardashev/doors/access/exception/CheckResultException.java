package ru.bardashev.doors.access.exception;

import ru.bardashev.doors.access.enumeration.CheckResultCode;

public class CheckResultException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    private CheckResultCode result;

    public CheckResultException(CheckResultCode result) {
        super();
        this.result = result;
    }

    public CheckResultException(CheckResultCode result, String message) {
        super(message);
        this.result = result;
    }

    public CheckResultCode getResult() {
        return result;
    }
}
