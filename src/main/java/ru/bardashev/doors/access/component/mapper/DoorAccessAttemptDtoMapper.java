package ru.bardashev.doors.access.component.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.ReportingPolicy;

import ru.bardashev.doors.access.domain.entity.DoorAccessAttempt;
import ru.bardashev.doors.access.enumeration.CheckResultCode;
import ru.bardashev.doors.access.web.model.dto.DoorAccessAttemptDto;

@Mapper(componentModel = "spring", unmappedSourcePolicy = ReportingPolicy.IGNORE, unmappedTargetPolicy = ReportingPolicy.ERROR)
public interface DoorAccessAttemptDtoMapper {

    @Mapping(target = "id", ignore = true)
    @Mapping(target = "accessDate", ignore = true)
    DoorAccessAttempt map(DoorAccessAttemptDto source, CheckResultCode result);
}
