package ru.bardashev.doors.access.web.controller;

import javax.validation.Valid;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import lombok.RequiredArgsConstructor;
import ru.bardashev.doors.access.service.DoorsService;
import ru.bardashev.doors.access.web.model.dto.DoorAccessAttemptDto;

@Validated
@RestController
@RequiredArgsConstructor
public class DoorsApiController {

    private final DoorsService service;

    @GetMapping(value = "/check")
    public ResponseEntity<Void> check(@Valid DoorAccessAttemptDto attemptDto) {
        service.checkAndGo(attemptDto);
        return ResponseEntity.ok().build();
    }
}
