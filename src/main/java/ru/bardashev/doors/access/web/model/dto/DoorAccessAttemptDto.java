package ru.bardashev.doors.access.web.model.dto;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import lombok.Data;


@Data
public class DoorAccessAttemptDto {

    @Min(1)
    @Max(5)
    @NotNull
    private Integer roomId;

    @Min(1)
    @Max(10000)
    @NotNull
    private Integer keyId;

    @NotNull
    private Boolean entrance;
}
