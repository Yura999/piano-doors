package ru.bardashev.doors.access.service.impl;

import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;

import ru.bardashev.doors.access.component.mapper.DoorAccessAttemptDtoMapperImpl;
import ru.bardashev.doors.access.domain.entity.DoorAccessAttempt;
import ru.bardashev.doors.access.domain.repository.DoorAccessAttemptRepository;
import ru.bardashev.doors.access.exception.CheckResultForbiddenException;
import ru.bardashev.doors.access.exception.CheckResultWrongDirectionException;
import ru.bardashev.doors.access.exception.CheckResultWrongRoomException;
import ru.bardashev.doors.access.web.model.dto.DoorAccessAttemptDto;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;


@ExtendWith(MockitoExtension.class)
public class DoorsServiceImplTest {

    @Mock
    private DoorAccessAttemptRepository repository;

    @Spy
    private DoorAccessAttemptDtoMapperImpl mapper;


    @InjectMocks
    private DoorsServiceImpl service;

    @Test
    void successEnterTest() {
        var attemptDto = new DoorAccessAttemptDto();
        attemptDto.setKeyId(8);
        attemptDto.setRoomId(4);
        attemptDto.setEntrance(true);
        service.checkAndGo(attemptDto);
        verify(repository, times(1)).save(any());
    }

    @Test
    void successExitTest() {
        Integer userId = 8;

        var attemptDto = new DoorAccessAttemptDto();
        attemptDto.setKeyId(userId);
        attemptDto.setRoomId(4);
        attemptDto.setEntrance(false);

        var last = new DoorAccessAttempt();
        last.setKeyId(userId);
        last.setRoomId(4);
        last.setEntrance(true);
        when(repository.findLastSuccessfulAttempt(eq(userId))).thenReturn(Optional.of(last));

        service.checkAndGo(attemptDto);
        verify(repository, times(1)).save(any());
    }

    @Test
    void forbiddenTest() {
        var attemptDto = new DoorAccessAttemptDto();
        attemptDto.setKeyId(7);
        attemptDto.setRoomId(4);
        attemptDto.setEntrance(true);
        assertThrows(CheckResultForbiddenException.class, () -> service.checkAndGo(attemptDto));
        verify(repository, times(1)).save(any());
    }

    @Test
    void wrongEntranceTest() {
        Integer userId = 8;

        var attemptDto = new DoorAccessAttemptDto();
        attemptDto.setKeyId(userId);
        attemptDto.setRoomId(4);
        attemptDto.setEntrance(true);

        var last = new DoorAccessAttempt();
        last.setRoomId(2);
        last.setEntrance(true);
        when(repository.findLastSuccessfulAttempt(eq(userId))).thenReturn(Optional.of(last));

        assertThrows(CheckResultWrongDirectionException.class, () -> service.checkAndGo(attemptDto));
        verify(repository, times(1)).save(any());
    }

    @Test
    void wrongEntranceFirstTest() {
        Integer userId = 8;

        var attemptDto = new DoorAccessAttemptDto();
        attemptDto.setKeyId(userId);
        attemptDto.setRoomId(4);
        attemptDto.setEntrance(false);

        when(repository.findLastSuccessfulAttempt(eq(userId))).thenReturn(Optional.empty());

        assertThrows(CheckResultWrongDirectionException.class, () -> service.checkAndGo(attemptDto));
        verify(repository, times(1)).save(any());
    }

    @Test
    void wrongRoomTest() {
        Integer userId = 8;

        var attemptDto = new DoorAccessAttemptDto();
        attemptDto.setKeyId(userId);
        attemptDto.setRoomId(4);
        attemptDto.setEntrance(false);

        var last = new DoorAccessAttempt();
        last.setKeyId(userId);
        last.setRoomId(2);
        last.setEntrance(true);
        when(repository.findLastSuccessfulAttempt(eq(userId))).thenReturn(Optional.of(last));

        assertThrows(CheckResultWrongRoomException.class, () -> service.checkAndGo(attemptDto));
        verify(repository, times(1)).save(any());
    }
}
